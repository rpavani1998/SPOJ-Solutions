(ns clojure.PRIME1)

(defn prime? [n]
  (if (and (not= 2 n) (even? n)) false
      (let [root (num (int (Math/sqrt n)))]
	(loop [i 3]
	  (if (> i root) true
	      (if (zero? (mod n i)) false
		  (recur (+ i 2)))))))
  )

(defn primes [start end]
  (filter #(prime? (range start (inc end))))
  )

(defn PRIME1 [n]
  (let [input (read-line) [start end] (clojure.string/split input #" ")]
    (when (> n 0)
      (println (primes (read-string start) (read-string end)))
      (PRIME1 (dec n)))
  ))


(PRIME1 (read-string (read-line)))
