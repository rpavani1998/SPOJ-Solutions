(ns clojure.TEST)

(defn parse-int [s]
   (Integer. (re-find  #"\d+" s )))

(defn TEST []
(doseq [line (line-seq (java.io.BufferedReader. *in*))]
    (if (= (parse-int line) 42) (System/exit 0)
    (println line))
  ))

(TEST)
