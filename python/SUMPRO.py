for t in range(int(input())):
	num = int(input())
	lst = [ num // i for i in range(1, num + 1)]
	print(sum(lst[i] * (i+1) for i in range(len(lst))) % 1000000007)

