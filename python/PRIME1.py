import sys
import math

def isPrime(n):
	if n == 0 or n == 1:
		return False
	elif n == 2:
		return True
	elif n % 2 == 0:
		return False
	else:
		for i in range(3,int(math.sqrt(n)) + 1,2):
			if n % i == 0:
				return False
	return True


n = int(input())

for i in range(n):
	start,end = map(int,input().split())
	for i in range(start,end + 1):
		if isPrime(i):
			print(i)
	print()
