from sys import stdin

input()
for N in map(int, stdin):
    count, num = 0, 5
    while num <= N:
        count += N//num
        num *= 5
    print(count)